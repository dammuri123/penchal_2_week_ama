# 1. Why using enumerate(list) is better than using range(len(list)) ?
+ enumerate() is faster when you want to repeatedly access the list/iterable items at their index.
+ enumerate() provides more readability.
+ Using `range(len(list))` involves calling `len(list)`, which adds an extra function call.(Avoding `len` calculation.)

**Example:**

**Using `enumerate`:**
```js
names = ["Alice", "Bob", "Charlie"]
for index, name in enumerate(names):
    print(index, name)
```
**Using `range(len(list))`:**
```js
names = ["Alice", "Bob", "Charlie"]
for index in range(len(names)):
    print(index, names[index])
```
# 2. What is difference between innerText and innerHTML in js?
### innerText:
+ It represents the text content of an element. It includes only the text that is visible to the user.
+ It ignores the HTML tags.

**Example:**
```HTML
<div id="example">
  Hello <b>World</b>!
</div>
```
```js
document.getElementById("example").innerText;
// Output: "Hello World!"
```
### innerHTML:
+ It represents the HTML content of an element. It includes both the text content and the HTML tags.
+ When you use `innerHTML`, it returns a string that includes any HTML tags within the element.
+  It can be used to get or set the HTML markup contained within the element.

**Example:**
```HTML
<div id="example">
  Hello <b>World</b>!
</div>
```
```js
document.getElementById("example").innerHTML;
// Output: "Hello <b>World</b>!"
```
# 3. Why we use groupBy in SQL?
+ In SQL, we use the `GROUP BY` clause to group rows that have the same values in specified columns into summary rows. 
+ This is especially useful when you want to perform aggregate functions (like COUNT, SUM, AVG, MAX, MIN) on each group of rows.
# 4. What is destructuring in JS?
+ Destructuring in JavaScript is a way to extract values from arrays or properties from objects into distinct variables.
+ It allows you to break down complex structures into simpler parts, making your code cleaner and easier to read.
### Example:

**Destructuring Arrays:**
```js
const fruits = ['apple', 'banana', 'cherry'];

// Without destructuring
const first = fruits[0];
const second = fruits[1];

// With destructuring
const [firstFruit, secondFruit] = fruits;

console.log(firstFruit); // 'apple'
console.log(secondFruit); // 'banana'
```
**Destructuring Objects:**
```js
const person = {
    name: 'Alice',
    age: 25,
    city: 'Wonderland'
};

// Without destructuring
const name = person.name;
const age = person.age;

// With destructuring
const { name, age } = person;

console.log(name); // 'Alice'
console.log(age); // 25
```
# 5. What does git clone do?
+ The `git clone` command is used to create a copy of an existing Git repository.
+ It allows you to download all the files, branches, and commit history from a remote repository to your local machine.

# 6. Difference between set and dictionary?
### set:
+ set is an unordered collection datatype.
+ set doesnot allows duplicates.
+ set it a mutable.
+ In set, the elements are stored in curly braces {}.

**Syntax:**
```python
{1, 2, 3} or set([1, 2, 3])
```
### dictionary:
+ It stores key-value pairs.
+ Unordered collection (ordered from Python 3.7+).
+ Keys must be unique.
+ In dictionary, the elements are stored in curly braces {}.

**Syntax:**
```python
{'key1': 'value1', 'key2': 'value2'} or dict(key1='value1', key2='value2')
```
# 7. What is position property in css and what is the difference between them?
+ The `position` property in CSS is used to specify how an element is positioned in a document.
## Summary of Differences:
### static: 
+ HTML elements are positioned static by default.
+ Static positioned elements are not affected by the top, bottom, left, and right properties.
### relative:
+ Positioned relative to its normal position. It does not affect the layout of other elements.
### absolute:
+ Positioned relative to the nearest positioned ancestor. 
+ Absolute positioned elements are removed from the normal flow, and can overlap elements.
### fixed: 
+ Positioned relative to the viewport. Stays fixed when scrolling.
### sticky:
+ Toggles between relative and fixed positioning based on scroll position.

# 8. What is the difference between 'inline-flex' and 'inline' in css?
### inline:
  + When we put `display: inline`, It act's as an inline elements.
  + Displaying multiple elements on the same line without starting a new line.
  + They only take up as much width as necessary and cannot have width, height, padding, or margins applied to them vertically.
### inline-flex:
+ When we put `display: inline-flex`, It behave like inline elements but are also flexible containers like flex.
+ They remain inline-level elements but can use flex properties like `justify-content` and `align-items` to control the layout of their children.

# 9. what is lexical scope in js?
+ Lexical scope is a fundamental concept in programming that determines the accessibility of variables and functions within a program.
+ The lexical scope is the scope of a variable or function based on where it is defined in the source code.


**Example:**
```js
function outerFunction() {
    var outerVar = 'I am from the outer scope';

    function innerFunction() {
        var innerVar = 'I am from the inner scope';
        console.log(outerVar); // Can access outerVar
        console.log(innerVar); // Can access innerVar
    }

    innerFunction();
    // console.log(innerVar); // Error: innerVar is not defined
}

outerFunction();
```

# 10. what is classList attribute in DOM?
+ The classList property is a read-only property.
+ The HTML DOM classList property is used to return the class names associated with an HTML element.
+ It returns the class names in the form of a DOMTokenlist .
+ The DOMTokenlist is nothing but a set of space-seperated tokens. This property is useful in adding, removing or toggling css classes of an element.








