## 1. What is a prompt box in JS?
+ A prompt box, commonly known as a "prompt dialog box" or simply "prompt," is a user interface element typically used in web development to interact with users.
+ It is a pop-up box that prompts the user to enter input, such as text or numbers, which can then be used within a script or application.
+ Prompt boxes are often used for collecting simple user input without the need for a dedicated input form.
## 2. what is slicing in Python?
+ In Python, slicing refers to the technique of extracting a portion of a sequence (such as a list, tuple, or string) by specifying a range of indices. It allows you to create a new sequence containing only the elements within that range.

**Syntax:**
```python
variablename[startindex:endindex:Updation]
```
**Example:**
```python
str = 'Hai Penchalaiah'
print(str[4:11:1])

O/P: penchal
```
## 3. What is isNaN function?
+ In JavaScript, the isNaN function is used to determine whether a value is NaN (Not-a-Number).

**Syntax:**
```javascript
isNaN(value)
```

**Examples:**
```javascript
isNaN(NaN);        // true
isNaN(123);        // false
isNaN("123");      // false
isNaN("abc");      // true
isNaN(true);       // false
isNaN(undefined);  // true
isNaN(null);       // false
```
## 4. what is INNER JOIN and OUTER JOIN in SQL?
**Inner Join:**
+ INNER JOIN returns the common and the matching records between the tables.
+ SQL inner joins are slower.
+ Matching records are returned in an INNER JOIN based on common fields or columns.
+ There is no inner join variation.

**Outer Join:**
+ OUTER JOIN returns all the records from the database tables.
+ Outer joins are faster as compared to inner joins.
+ Because all records are returned, the OUTER JOIN does not require a common column ID.
+ An outer join is either a left join, a right join, or a full join (cross join).
## 5.  what are Promise.resolve() and Promise.reject() in JS?
`Promise.resolve()` and `Promise.reject()` are both methods provided by the Promise object in JavaScript. They serve different purposes in handling asynchronous operations:

**Promise.resolve():**
+ Creates a new Promise object that is resolved with the provided value or a resolved Promise object if the provided value is already a Promise.

**Example:**
```javascript
const resolvedPromise = Promise.resolve('Resolved');
resolvedPromise.then(console.log); // Output: Resolved
```
**Promise.reject():**
+ Creates a new Promise object that is rejected with the provided reason.
+ Useful for creating a rejected Promise object explicitly to signify failure or error conditions.
+ Used when you want to handle error scenarios or explicitly reject a promise.

**Example:**
```javascript
const rejectedPromise = Promise.reject('Rejected');
rejectedPromise.catch(console.error); // Output: Rejected
```
## 6. What is promise chain?
+ A Promise chain refers to a sequence of asynchronous operations linked together using Promise methods like `.then()` and `.catch().` In a Promise chain, each operation depends on the result of the previous one, creating a logical flow of asynchronous tasks.
## 7. Why must .catch be placed toward the end of the promise chain?
+ In a Promise chain, the .catch() block is typically placed towards the end of the chain for several reasons:

**1. Error Propagation:** 
+ Placing `.catch()` at the end of the chain ensures that any errors occurring in preceding `.then()` blocks are caught before they propagate further up the chain. This helps prevent unhandled promise rejections and ensures robust error handling.

**2. Readability and Maintainability:**
+ Putting `.catch()` at the end of the chain makes the code more readable and maintainable. It clearly indicates where error handling occurs in the Promise chain, making it easier for developers to understand and modify the code.

**3. Logical Flow:**
+ Error handling is often considered a separate concern from the main logic of the Promise chain. By placing `.catch()` at the end, you separate the error handling logic from the main execution flow, improving code organization and clarity.

**4. Consistency:**
+ Placing `.catch()` at the end of the chain follows a common convention used in Promise-based code, making the code more consistent and easier for other developers to understand. It helps maintain consistency in error handling across different parts of the codebase.
## 8.  How to move head to previous commit?
+ To move the HEAD pointer to the previous commit in Git, you can use the git checkout command along with the commit hash
```git
git checkout <commit-hash>
```
or
Using Relative Reference:
```git 
git checkout HEAD^
```
or 
```git
git checkout HEAD~1
```
## 9. what is Global execution context?
+ The Global Execution Context is created when the JavaScript engine starts executing the script file.
+ It is the outermost context and serves as the default context for any code that is not inside a function.
## 10. what is .then() and .catch() in promise?
+ In JavaScript, `.then()` and `.catch()` are methods associated with Promises, which are used for handling asynchronous operations.

**.then():**
+ The `.then()` method is used to specify what should happen once a Promise is resolved.

**.catch():**
+ The `.catch()` method is used to specify what should happen if the Promise is rejected. It is typically used for error handling.  


