# 1. What are http headers?
+ An HTTP header is a field of an HTTP request or response that passes additional context and metadata about the request or response.

## Types of headers:
### Request header:
+ Headers containing more information about the resource to be fetched or about the client itself.
### Response header:
+ Headers with additional information about the response, like its location or about the server itself (name, version, …).
### Representation header:
+ metadata about the resource in the message body (e.g. encoding, media type, etc.).
### Fetch metadata request header:
+ Headers with metadata about the resource in the message body (e.g. encoding, media type, etc.).

# 2. What are decorators?
+ Decorators are very powerful and useful feature in python.
+ Decorators are used for adding additional features and modifying the behaviours of the functions or classes.
### Properties of decorator function:
+ It should be a nested function.
+ Outer function should carry one mandatory argument.
+ Outer function should return inner function address.

### Example:
```python
def my_decorator(func):
    def wrapper():
        print("Something is happening before the function is called.")
        func()
        print("Something is happening after the function is called.")
    return wrapper

@my_decorator  
def say_hello():
    print("Hello!")

say_hello()

O/P: 
Something is happening before the function is called.
Hello!
Something is happening after the function is called.
```
# 3. Discuss Django’s Request/Response Cycle?
The request-response cycle in Django consists of four main steps:
1. The user sends a request to the web server.
2. The server receives the request and passes it to Django.
3. Django processes the request and generates a response.
4. Finally, the server sends the response back to the user.

# 4. Difference between Python module and Python class?
Here's the difference between a Python module and a class in simple terms:

**Module:**

* **Toolbox:** A module is like a toolbox containing a collection of reusable functions, variables, and even classes.
* **Organization:** It helps organize your code by grouping related functionalities together in a single file (`.py`).
* **Sharing:** You can import modules into your program to use the functions and variables they provide.

**Class:**

* **Blueprint:** A class is a blueprint for creating objects. It defines the **properties** (variables) and **behaviors** (functions) that objects of that type will have.
* **Object creation:** You use a class to create multiple objects (instances) that share the same characteristics.
* **Uniqueness:** Each object can have its own unique set of values for its properties.

**In short:**

* A module is a collection of tools, while a class is a template to build specific objects.
# 5. How to filter items in the model?
+ The `filter()` method is used to filter your search, and allows you to return only the rows that matches the search term.

### Example:
```python
mydata = Member.objects.filter(firstname='Emil').values()

O/P: Return only the records where the firstname is 'Emil'
```
# 6. How can you handle multiple promises running in parallel but needing to process results sequentially?
+ Use `Promise.all()` to run multiple promises in parallel and then process the results sequentially in a `then` handler.

### Example:
```javascript
// Sample async functions that return promises
const asyncFunction1 = () => new Promise(resolve => setTimeout(() => resolve('Result 1'), 1000));
const asyncFunction2 = () => new Promise(resolve => setTimeout(() => resolve('Result 2'), 500));
const asyncFunction3 = () => new Promise(resolve => setTimeout(() => resolve('Result 3'), 2000));

// Run all promises in parallel
Promise.all([asyncFunction1(), asyncFunction2(), asyncFunction3()])
    .then(results => {
        // Process results sequentially
        return results.reduce((promiseChain, currentResult) => {
            return promiseChain.then(() => {
                console.log(currentResult);
                // Any other sequential processing can be done here
                return Promise.resolve();
            });
        }, Promise.resolve()); // Initial value for the promise chain
    })
    .catch(error => {
        console.error('One of the promises failed:', error);
    });
```
# 7. how do you obtain sql query from queryset in django?
+ By using `query` attrribute, we can get sql query from queryset.

### Example:
```python
from myapp.models import MyModel

# Create a queryset
queryset = MyModel.objects.filter(some_field='some_value')

# Get the raw SQL query
sql_query = str(queryset.query)
print(sql_query)
```
# 8. What are Q objects in django?
+ The 'Q' object is a way of defining queries that can be combined using logical operators to create more complex queries.
+ 'Q' object encapsulates a SQL expression in a Python object that can be used in database-related operations.

### Example:
```python
# Complex query combining AND, OR, and NOT
queryset = MyModel.objects.filter(
    Q(field1='value1') & (Q(field2='value2') | Q(field3='value3')) & ~Q(field4='value4')
)
```
# 9. What are fields in django?
+ In Django, fields are used to define the structure and type of data that can be stored in a database model.
+ Django provides a variety of field types to handle different kinds of data, such as strings, numbers, dates, and relationships between models.
+ For example, to store an integer, IntegerField would be used.
### Example for IntegerField:
```python
# Used to store integers
class MyModel(models.Model):
    age = models.IntegerField()
```
# 10. Describe some best practices for designing RESTful APIs?
+ Use HTTP methods like `GET`, `PUT`, `POST`, and `DELETE` for actions.
+ Use Status codes for responses.
+ Use JSON for data format.
+ Use query parameters for filtering and sorting.
+ Use Caching.



