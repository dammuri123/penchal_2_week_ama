## 1. What is a primary key in a table?
* Primary Key is a constraint is used to identify each row in a table uniquely.
* It is a combination of not null and unique.
* For one table, we can have only one primary key.
## 2. what is Having clause?
* In SQL, the HAVING clause is used together with the GROUP BY clause to filter rows returned by a GROUP BY query based on specified conditions.
* we cannot use where codition for multi-row functions instead of that we can use having clause.
## 3. What is limit and offset in SQL?
In SQL, `LIMIT` and `OFFSET` are used together to control the number of rows returned by a query and where to start returning them from.

- **LIMIT**: Specifies the maximum number of rows to return in the result set.
- **OFFSET**: Specifies the number of rows to skip before starting to return rows.

For example, suppose you have a table of customers and you want to retrieve only the first 10 customers, starting from the 6th row:

```sql
SELECT * FROM customers
LIMIT 10 OFFSET 5;
```
## 4. What are views in SQL?
* Views are used to maintain a logical copy of data that is required.
* It is used to display the necessary data by hidding the un-necessary data.

**Single-view:** Contains only one base table.

**Complex-view:** Contains more than one base table.

**Advantages:**
* It Provides security for the data.
* Increasing the database performance.

**Syntax:**
```sql
create view viewname as select columnname from tablename;
```
## 5. What are the different types of command in SQL?
1. **Data Definition Language (DDL) Commands**:
   - **CREATE**: Used to create database objects like tables, views, indexes, etc.
   - **ALTER**: Modifies existing database objects, such as adding, modifying, or dropping columns in a table.
   - **DROP**: Deletes database objects like tables, views, indexes, etc.
   - **TRUNCATE**: Removes all records from a table but keeps the table structure intact.
   - **RENAME**: Renames an existing database object.

2. **Data Manipulation Language (DML) Commands**:
   - **SELECT**: Retrieves data from one or more tables.
   - **INSERT**: Adds new rows of data into a table.
   - **UPDATE**: Modifies existing data in a table.
   - **DELETE**: Removes one or more rows from a table.
   - **MERGE**: Performs an INSERT, UPDATE, or DELETE operation based on specified conditions.

3. **Data Control Language (DCL) Commands**:
   - **GRANT**: Provides specific privileges to users or roles.
   - **REVOKE**: Revokes previously granted privileges from users or roles.

4. **Transaction Control Commands**:
   - **COMMIT**: Saves all changes made in the current transaction.
   - **ROLLBACK**: Reverts all changes made in the current transaction.
   - **SAVEPOINT**: Sets a point within a transaction to which you can later roll back.
   - **SET TRANSACTION**: Sets characteristics for the transaction like isolation level or access mode.

## 6. Name some of the built-in module in python?
1. **math**: For mathematical operations like trigonometry, logarithms, and constants.
   
2. **random**: For generating random numbers and making random selections.
   
3. **os**: For interacting with the operating system, such as file operations, directory operations, and environment variables.
   
4. **sys**: For accessing system-specific parameters and functions, such as command-line arguments and the Python interpreter.
   
5. **datetime**: For working with dates, times, and time intervals.
   
6. **json**: For encoding and decoding JSON data.
   
7. **re**: For working with regular expressions, allowing pattern matching and manipulation of strings.
   
8. **csv**: For reading and writing CSV (Comma Separated Values) files.
   
9. **collections**: For additional data structures like OrderedDict, defaultdict, Counter, etc.
   
10. **pickle**: For serializing and deserializing Python objects, enabling data persistence.

## 7. How do you make sure your webpage is responsive?
If we create webpage, then the following ways we can ensure that the our page is responsive:
1. By using media Queries.
2. By using responsive framework like Boostrap.
3. By testing Across Devices.
4. By Setting images and media to max-width: 100%
5. By using the viewport meta tag in the head section of our HTML to control the viewport's width and scale on different devices.
6. By designing our layouts using percentages or relative units like em or rem instead of fixed pixel values.
## 8. Difference between find and index methods in python.
**index():**
- It is used for returning the index position if value is present else it returns error as output.

**find()**:
- It is used for returning the index position if value is present else it returns "-1" as output.
## 9. If you have employee table and want to change the salary of an employee with id of 100 in table?
- By using UPDATE Command and where condition, we can do it.

Here's an example,
```sql
UPDATE employee
SET salary = new_salary_value
WHERE employee_id = 100;
```
## 10. What is the DOM?
DOM stands for Document Object Model.It's a programming interface for web documents, such as HTML, XML, and SVG documents.In simple terms, the DOM represents the structure of a document as a tree-like model, where each node represents a part of the document (like elements, attributes, and text), and these nodes can be manipulated using programming languages like JavaScript.
## 11. What are aliases in SQL?
* In SQL, an alias is a temporary name assigned to a table or a column in a query. 
* Aliases are commonly used to make column names more readable, to handle self-joins, or to distinguish between tables when multiple tables are involved in a query.

We have two types of aliases,

1. Table alias.

2. Column alias.

**Table alias:** A table alias is used to give a temporary name to a table in a SQL statement, especially when you need to reference the same table multiple times in a query or when joining tables.

**Example:**
```sql
SELECT e.first_name, d.department_name
FROM employees e
INNER JOIN departments d ON e.department_id = d.department_id;
```
**Column alias:** A column alias is used to give a temporary name to a column in the result set of a query. This can be helpful when you want to change the name of a column in the output without altering the original column name in the database.

**Example:**
```sql
SELECT first_name AS "First", last_name AS "Last"
FROM employees;
```

